FROM openjdk:8
EXPOSE 8080
ADD target/spring-boot-hibernate-docker.jar
ENTRYPOINT ["java","-jar","/spring-boot-hibernate-docker"]