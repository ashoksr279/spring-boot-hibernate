package com.java.sample.springboothibernate.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.java.sample.springboothibernate.dao.EmployeeDAO;
import com.java.sample.springboothibernate.model.Employee;

@Repository
public class EmployeeDAOImpl implements EmployeeDAO {
	
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public List<Employee> getEmployees() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Employee> query = currentSession.createQuery("from Employee", Employee.class);
		return query.getResultList();
	}

	@Override
	public Employee get(int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		return currentSession.get(Employee.class, id);
	}

	@Override
	public void save(Employee emp) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(emp);
	}

	@Override
	public Employee delete(int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Employee obj = currentSession.get(Employee.class, id);
		currentSession.delete(obj);
		return obj;
	}

}
