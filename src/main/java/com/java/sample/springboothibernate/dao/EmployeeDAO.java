package com.java.sample.springboothibernate.dao;

import java.util.List;

import com.java.sample.springboothibernate.model.Employee;

public interface EmployeeDAO {
	List<Employee> getEmployees();
	Employee get(int id);
	void save(Employee emp);
	Employee delete(int id);
}
