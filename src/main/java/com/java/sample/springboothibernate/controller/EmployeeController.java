package com.java.sample.springboothibernate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.sample.springboothibernate.model.Employee;
import com.java.sample.springboothibernate.service.EmployeeService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/restapi")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@GetMapping("/get")
	@ApiOperation(value = "get all the employees list")
	public List<Employee> getEmployeeList(){
		return employeeService.getEmployees();
	}
	
	@PostMapping("/save")
	public String saveEmployee(@RequestBody Employee emp){
		employeeService.save(emp);
		return "saved successfully";
	}
	
	@GetMapping("/getEmployee/{id}")
	@ApiOperation(value = "get employee by Id")
	public Employee getEmployee(@PathVariable int id){
		return employeeService.get(id);
	}
	
	@DeleteMapping("/deleteEmployee/{id}")
	@ApiOperation(value = "delete employee by Id")
	public Employee deleteEmployee(@PathVariable int id){
		return employeeService.delete(id);
	}
	
	@PostMapping("/update")
	@ApiOperation(value = "update employee by Id")
	public String updateEmployee(@RequestBody Employee emp){
		employeeService.save(emp);
		return "Update Successfully";
	}

}
