package com.java.sample.springboothibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
public class SpringBootHibernateApplication {

	public static void main(String[] args) {
                System.out.println("Added For Testing...");
		SpringApplication.run(SpringBootHibernateApplication.class, args);
	}

}
