package com.java.sample.springboothibernate.service;

import java.util.List;

import com.java.sample.springboothibernate.model.Employee;

public interface EmployeeService {
	List<Employee> getEmployees();
	Employee get(int id);
	void save(Employee emp);
	Employee delete(int id);
}
