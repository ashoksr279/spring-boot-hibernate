package com.java.sample.springboothibernate.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.java.sample.springboothibernate.dao.EmployeeDAO;
import com.java.sample.springboothibernate.model.Employee;
import com.java.sample.springboothibernate.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	private EmployeeDAO employeeDao;
	
	@Transactional
	@Override
	public List<Employee> getEmployees() {
		return employeeDao.getEmployees();
	}

	@Transactional
	@Override
	public Employee get(int id) {
		return employeeDao.get(id);
	}

	@Transactional
	@Override
	public void save(Employee emp) {
		employeeDao.save(emp);
		
	}

	@Transactional
	@Override
	public Employee delete(int id) {
		return employeeDao.delete(id);
	}

}
